import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeRootComponent } from './home-root.component';
import {AnimationModule} from "../shared/modules/animation/animation.module";
import {MatButtonModule} from "@angular/material/button";


@NgModule({
  declarations: [
    HomeRootComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    AnimationModule,
    MatButtonModule
  ]
})
export class HomeModule { }
