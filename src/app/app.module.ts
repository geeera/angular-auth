import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AnimationModule } from "./shared/modules/animation/animation.module";
import { SharedModule } from "./shared/shared.module";
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    HttpClientModule,
    SharedModule,
    AnimationModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
