import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarRef } from '@angular/material/snack-bar';
import { SnackComponent } from '../modules/snack/snack.component';
import { v4 } from 'uuid';
import {BehaviorSubject, Observable} from 'rxjs';
import {MainAnimateIconColor} from '../modules/animation/animate-icon/animate-icon.component';

export interface ISnackData {
  message: string;
  iconName: string;
}

export interface ISnackMessage {
  id: string;
  text: string;
  dateCreated: Date | number;
  dateExpired: Date | number;
  color?: MainAnimateIconColor;
  iconName?: string;
}

export type SnackBarType = 'success' | 'wrong' | 'danger';

type SnackBarDurationType = number | 'infinity' | undefined;

@Injectable({
  providedIn: 'root'
})
export class SnackService {
  private defaultConfig: MatSnackBarConfig = {};
  private messages: ISnackMessage[] = [
    {
      text: 'First Snack bar message',
      dateCreated: Date.now(),
      dateExpired: Date.now() + (3600 * 1000),
      id: 'snack-message-1',
      color: 'primary',
      iconName: 'check'
    }
  ];
  private messages$: BehaviorSubject<ISnackMessage[]> = new BehaviorSubject(this.messages);

  get messageSub(): Observable<ISnackMessage[]> {
    return this.messages$.asObservable();
  }

  constructor(
    private snack: MatSnackBar,
  ) { }

  addMessage(data: ISnackData, color: MainAnimateIconColor): void {
    this.messages.push({
      color,
      text: data.message,
      iconName: data.iconName,
      dateCreated: Date.now(),
      dateExpired: Date.now() + (3600 * 1000),
      id: v4()
    });
    this.messages$.next(this.messages);
  }

  deleteMessage(messageId: string): void {
    this.messages = this.messages.filter(message => message.id === messageId);
    this.messages$.next(this.messages);
  }

  clearMessages(messageIds: string[]): void {
    this.messages = this.messages.filter(message => messageIds.includes(message.id));
    this.messages$.next(this.messages);
  }

  clearMessagesByDate(): void {
    this.messages = this.messages.filter(message => message.dateExpired < Date.now());
    this.messages$.next(this.messages);
  }

  getColor(type: SnackBarType): MainAnimateIconColor {
    if (type === 'success') {
      return 'primary';
    } else if (type === 'wrong') {
      return 'accent';
    } else if (type === 'danger') {
      return 'warn';
    }
    return 'primary';
  }

  create(data: ISnackData, type: SnackBarType, duration: number | null): MatSnackBarRef<SnackComponent> {
    this.addMessage(data, this.getColor(type));
    const config = {
      ...this.defaultConfig,
      panelClass: ['snack-bar-wrapper', type],
      data,
    };

    if (duration) {
      config['duration'] = duration;
    }

    return this.snack.openFromComponent(SnackComponent, config);
  }

  success(message: string, duration?: SnackBarDurationType): MatSnackBarRef<SnackComponent> {
    return this.create({ message: `${message} 😄`, iconName: 'check' }, 'success', this.takeDuration(duration));
  }

  wrong(message: string, duration?: SnackBarDurationType): MatSnackBarRef<SnackComponent> {
    return this.create({ message: `${message} 🤔`, iconName: 'bug' }, 'wrong', this.takeDuration(duration));
  }

  danger(message: string, duration?: SnackBarDurationType): MatSnackBarRef<SnackComponent> {
    return this.create({ message: `${message} 😱`, iconName: 'warning' }, 'danger', this.takeDuration(duration));
  }

  takeDuration(duration: SnackBarDurationType): number | null {
    if (duration === 'infinity') {
      return null;
    }

    return duration || 3000;
  }
}
