import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export const equalValidator = (first: string, second: string): ValidatorFn => {

  return (control: AbstractControl): ValidationErrors | null => {

    const password = control.get(first)?.value;
    const confirm = control.get(second)?.value;

    if (password !== confirm) {
      return { noMatch: true };
    }

    return null;
  };
};
