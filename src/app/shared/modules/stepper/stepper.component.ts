import {
  AfterContentChecked,
  AfterContentInit, AfterViewChecked,
  AfterViewInit,
  Component,
  ContentChildren,
  Input,
  OnInit,
  QueryList,
  ViewChild
} from '@angular/core';
import {interval, Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {StepDirective} from './step.directive';

@Component({
  selector: 'stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit, AfterContentChecked, AfterViewChecked {
  @ContentChildren(StepDirective) steps!: QueryList<StepDirective>;
  @ViewChild('slider') slider: any;

  @Input() showDots = true;
  @Input() selectedStep = 0;
  position = '0px';
  width = 0;

  get stepsAsArray(): StepDirective[] {
    return this.steps?.toArray();
  }

  get leftPosition(): string {
    this.position = `${-(this.width * this.selectedStep)}px`;
    return this.position;
  }

  get isLastSelectedStep(): boolean {
    return this.stepsAsArray?.length - 1 === this.selectedStep;
  }

  get isFirstSelectedStep(): boolean {
    return this.selectedStep === 0;
  }

  get canBeStepperIndexChanged(): boolean | null {
    if (!this.stepsAsArray?.length) {
      return null;
    }
    return this.stepsAsArray[this.selectedStep + 1]?.isValid;
  }

  constructor() {
  }

  ngOnInit(): void {}

  ngAfterContentChecked(): void {
    this.selectedStep = this.selectedStep > this.stepsAsArray.length - 1
      ? this.stepsAsArray.length - 1
      : this.selectedStep;
  }

  ngAfterViewChecked(): void {
    this.setWidth();
  }

  setWidth(): void {
    this.width = this.slider?.nativeElement.clientWidth || 0;
  }

  changeStep(index: number): void {
    if (!this.width) {
      this.setWidth();
    }

    if (this.canBeStepperIndexChanged || index <= this.selectedStep) {
      this.selectedStep = index;
    }
  }

  next(): void {
    if (this.selectedStep < (this.stepsAsArray.length - 1)) {
      const activeStepIndex = this.selectedStep + 1;
      this.changeStep(activeStepIndex);
    }
  }

  prev(): void {
    if (this.selectedStep > 0) {
      const activeStepIndex = this.selectedStep - 1;
      this.changeStep(activeStepIndex);
    }
  }

  infinity(): Observable<number> {
    return interval(2000)
      .pipe(
        tap(() => {
          const activeStepIndex = this.selectedStep < (this.stepsAsArray.length - 1)
            ? this.selectedStep + 1 : 0;
          this.changeStep(activeStepIndex);
        }),
        map(() => this.selectedStep)
      );
  }

}
