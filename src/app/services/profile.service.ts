import { Injectable } from '@angular/core';
import { usersMock } from '../shared/mocks/users.mock';
import {IUser, IUserLogin, IUserSignUp} from '../shared/interfaces/user.interface';
import {SnackService} from '../shared/services/snack.service';
import {Router} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  private users = usersMock;
  private users$ = new BehaviorSubject<IUser[]>(this.users);

  private localUserIdKey = 'auth-user-id';
  private localUserTokenExpirationKey = 'auth-user-token-expiration';
  private expiredTime = 1.08e+7; /** 3 Hours */

  get userId(): string | null {
    return localStorage.getItem(this.localUserIdKey);
  }

  get isValidToken(): boolean {
    const nowTime = Date.now();
    const tokenExpire = Number(localStorage.getItem(this.localUserTokenExpirationKey));
    return nowTime < tokenExpire;
  }

  get observer(): Observable<IUser[]> {
    return this.users$.asObservable();
  }

  constructor(
    private notify: SnackService,
    private router: Router
  ) { }

  login(values: IUserLogin): boolean | null {
    const user = this.users.find(user => user.email === values.email);
    if (!user) {
      this.notify.danger('Sorry, we couldn\'t find this user' );
      return false;
    }

    if (user.password !== values.password) {
      this.notify.danger('Your password isn\'t valid' );
      return false;
    }

    this.notify.success(`${user.firstName}, well come`);
    this.generateToken(user.id, values.rememberMe);
    this.redirectToApp();
    return null;
  }

  sign_up(values: IUserSignUp): void {
    const { avatar, ...otherValues } = values;
    const user: IUser = {
      ...otherValues,
      id: `user-${this.users.length}`,
      avatarUri: avatar.name,
      isEmailConfirmed: false
    };

    this.users.push(user);
    this.notify.success(`${user.firstName}, well come`);
    this.generateToken(user.id);
    this.redirectToApp();
  }

  logout(): void {
    localStorage.removeItem(this.localUserIdKey);
    localStorage.removeItem(this.localUserTokenExpirationKey);
    this.router.navigate(['/auth']);
  }

  profile(): Observable<IUser | null> {
    const authorizedUserId = localStorage.getItem(this.localUserIdKey);
    return this.observer
      .pipe(map(users => users.find(user => user.id === authorizedUserId) || null));
  }

  private generateToken(userId: string, isNeedToRemember?: boolean): void {
    let tokenExpiration = Date.now() + this.expiredTime;

    if (isNeedToRemember) {
      tokenExpiration *= 4;
    }

    localStorage.setItem(this.localUserIdKey, userId);
    localStorage.setItem(this.localUserTokenExpirationKey, tokenExpiration.toString());
  }

  private redirectToApp(): void {
    this.router.navigate(['/']);
  }
}
