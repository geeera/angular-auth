import {animate, state, style, transition, trigger} from '@angular/animations';

export default [
  trigger('link', [
    state('true', style({ width: '*' })),
    state('false', style({ width: '44px' })),
    transition('false <=> true', animate(200))
  ])
];
