import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, NgZone, OnInit, Output} from '@angular/core';
import {AnimationOptions, LottieComponent} from 'ngx-lottie';
import {AnimationItem} from 'lottie-web';
import {BehaviorSubject} from 'rxjs';

export type AnimateIconColor = 'light' | 'dark' | 'primary' | 'accent' | 'warn' | 'invalid' | 'low' | 'medium' | 'strong';
export type MainAnimateIconColor = 'primary' | 'accent' | 'warn';

@Component({
  selector: 'animate-icon',
  templateUrl: './animate-icon.component.html',
  styleUrls: ['./animate-icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AnimateIconComponent implements OnInit {
  @Input() iconName: string = '';
  @Input() options: AnimationOptions = {};
  @Input() color: AnimateIconColor = 'dark';
  @Input() width = '24px';
  @Input() height = '24px';
  @Input() withStroke = false;
  @Input() loop = false;
  @Input() isActive = false;
  @Input() disabled = false;

  @Output() animateWasCreated = new EventEmitter<boolean>();

  animationItem!: AnimationItem;
  isAnimated$ = new BehaviorSubject(true);

  configurationOptions: AnimationOptions = {};
  defaultStyles: Partial<CSSStyleDeclaration> = {};

  get animationCanBeHovered(): boolean {
    return !(this.isActive || this.disabled);
  }

  constructor(
    private ngZone: NgZone,
    private ref: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.updateConfig();
    this.setDefaultStyles();
  }

  generateFullPath(iconName: string): string {
    return `assets/animated-icons/${iconName}/${iconName}.json`;
  }

  updateConfig(): void {
    this.configurationOptions = {
      ...this.options,
      path: this.generateFullPath(this.iconName),
      loop: this.loop,
      autoplay: true
    };
  }

  setDefaultStyles(): void {
    this.defaultStyles = {
      minWidth: '24px',
      margin: '0 auto'
    };
  }

  animationCreated(icon: AnimationItem, lottie: LottieComponent): void {
    this.animationItem = icon;
    this.animateWasCreated.emit(true);
    lottie.complete.subscribe(() => {
      this.stop();
    });
  }

  animationStart(): void {
    this.isAnimated$.next(true);
    this.ref.detectChanges();
  }

  animationEnd(): void {
    this.isAnimated$.next(false);
    this.ref.detectChanges();
  }

  stop(): void {
    this.ngZone.runOutsideAngular(() => {
      this.animationEnd();
      this.animationItem.stop();
    });
  }

  play(): void | null {
    if (!this.animationCanBeHovered) {
      return null;
    }

    this.ngZone.runOutsideAngular(() => {
      this.animationStart();
      this.animationItem.play();
    });
  }

  error(err: any): void {
    console.error('Animate icon error:', err);
    this.animateWasCreated.emit(false);
  }
}
