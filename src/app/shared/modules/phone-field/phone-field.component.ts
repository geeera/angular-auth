import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {ThemePalette} from '@angular/material/core';

@Component({
  selector: 'phone-field',
  templateUrl: './phone-field.component.html',
  styleUrls: ['./phone-fieldcomponent.scss']
})
export class PhoneFieldComponent implements OnInit {
  @Input() formGroup!: FormGroup;
  @Input() codeCountryAlias = 'codeCountry';
  @Input() phoneNumberAlias = 'phone';
  @Input() readonly = false;
  @Input() color: ThemePalette = 'accent';


  constructor() { }

  ngOnInit(): void {
  }

}
