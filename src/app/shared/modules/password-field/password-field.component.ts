import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormGroup} from '@angular/forms';
import {Options, passwordStrength, Result} from 'check-password-strength';
import {AnimateIconColor} from '../animation/animate-icon/animate-icon.component';

enum StrengthPasswordTypes {
  INVALID = 'invalid',
  LOW = 'low',
  MEDIUM = 'medium',
  STRONG = 'strong'
}

const customStrengthOptions: Options<StrengthPasswordTypes> = [
  {
    id: 0,
    value: StrengthPasswordTypes.INVALID,
    minDiversity: 0,
    minLength: 0
  },
  {
    id: 1,
    value: StrengthPasswordTypes.LOW,
    minDiversity: 2,
    minLength: 6
  },
  {
    id: 2,
    value: StrengthPasswordTypes.MEDIUM,
    minDiversity: 3,
    minLength: 8
  },
  {
    id: 3,
    value: StrengthPasswordTypes.STRONG,
    minDiversity: 4,
    minLength: 10
  }
];

@Component({
  selector: 'password-field',
  templateUrl: './password-field.component.html',
  styleUrls: ['./password-field.component.scss']
})
export class PasswordFieldComponent implements OnInit {
  @Input() formGroup!: FormGroup;
  @Input() passwordAlias = 'password';
  @Input() label = 'Password';
  @Input() withStrengthChecking = false;

  hidePassword = true;
  strengthPasswordOption!: Result<StrengthPasswordTypes>;
  strengthPasswordTypes = StrengthPasswordTypes;

  get icon(): string {
    return this.hidePassword ? 'visibility_off' : 'visibility';
  }

  get type(): string {
    return this.hidePassword ? 'password' : 'text';
  }

  get strengthPasswordClass(): StrengthPasswordTypes | string {
    return this.strengthPasswordOption?.value || '';
  }

  get colorForStrengthIcon(): AnimateIconColor {
    return this.strengthPasswordOption?.value;
  }

  constructor() { }

  ngOnInit(): void {
    if (this.withStrengthChecking) {
      const control: AbstractControl | null = this.formGroup.get(this.passwordAlias);

      if (control) {
        this.strengthPasswordOption = this.checkStrengthPassword(control.value);

        control.valueChanges.subscribe(value => {
          this.strengthPasswordOption = this.checkStrengthPassword(value);
        });
      }
    }
  }

  toggleVisibility(): void {
    this.hidePassword = !this.hidePassword;
  }

  checkStrengthPassword(value: string): Result<StrengthPasswordTypes> {
    return passwordStrength(value, customStrengthOptions);
  }

}
